/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.broadcast.bracha_brb.env;


import max.model.broadcast.bracha_brb.message.BrachaAbstractMessageLogWriter;


public abstract class BrachaIntegerMessageLogWriter extends BrachaAbstractMessageLogWriter<Integer> {

    @Override
    public String innerMessageToString(Integer msg) {
        return String.valueOf(msg);
    }

}
