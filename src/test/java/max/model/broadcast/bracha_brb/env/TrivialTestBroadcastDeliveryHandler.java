/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.broadcast.bracha_brb.env;

import max.model.broadcast.abstract_reliable_broadcast.agent.BroadcastPeerAgent;
import max.model.broadcast.abstract_reliable_broadcast.behavior.ReliableBroadcastHandler;


/**
 * What happens in our basic broadcast test when a peer finally delivers the message that is reliably broadcast.
 *
 * @author Erwan Mahe
 */
public class TrivialTestBroadcastDeliveryHandler implements ReliableBroadcastHandler<Integer, BroadcastPeerAgent> {
    @Override
    public void handle(Integer integer, BroadcastPeerAgent broadcastPeerAgent, String relevantEnvironment) {
        broadcastPeerAgent.getLogger().info("peer " + broadcastPeerAgent.getName() + " got message " + integer + " on environment " + relevantEnvironment);
    }
}
