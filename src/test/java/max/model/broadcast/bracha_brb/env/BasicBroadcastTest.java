/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.broadcast.bracha_brb.env;

import static max.core.MAXParameters.clearParameters;
import static max.core.MAXParameters.setParameter;
import static max.core.test.TestMain.launchTester;

import java.math.BigDecimal;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import max.core.action.ACTakeRole;
import max.core.action.Plan;
import max.core.agent.ExperimenterAgent;
import max.core.agent.MAXAgent;
import max.core.scheduling.ActionActivator;
import max.model.broadcast.abstract_reliable_broadcast.action.config.AcSetBroadcastDeliveryHandler;
import max.model.broadcast.abstract_reliable_broadcast.action.logic.AcStartBroadcastOfMessage;
import max.model.broadcast.abstract_reliable_broadcast.agent.BroadcastPeerAgent;
import max.model.broadcast.abstract_reliable_broadcast.role.RBroadcastPeer;
import max.model.broadcast.bracha_brb.action.AcSetPeerBroadcastApprovalPolicy;
import max.model.broadcast.bracha_brb.action.AcSetPeerBroadcastByzantineThreshold;
import max.model.broadcast.bracha_brb.action.AcSetPeerBroadcastLocalState;
import max.model.broadcast.bracha_brb.state.DefaultLocalBrbNodeState;
import max.model.network.stochastic_adversarial_p2p.action.config.AcAddCommunicationDelay;
import max.model.network.stochastic_adversarial_p2p.action.config.AcConfigMessageLogger;
import max.model.network.delay_distributions_lib.distros.ContinuousUniformDelay;
import max.model.network.stochastic_adversarial_p2p.context.filter.UniformLevelFilter;
import max.model.network.stochastic_adversarial_p2p.env.StochasticP2PEnvironment;
import max.model.network.stochastic_adversarial_p2p.role.RStochasticNetworkPeer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.io.TempDir;

/**
 * Basic Test in which we simply instantiate 4 peers in:
 * - a delivery environment which is application-side and on which the delivery of reliably broadcast messages has an effect
 * - a Bracha BRB environment in which the Bracha BRB protocol takes place
 *
 * One of the peers then initiate the reliable broadcast of the integer value 1
 *
 * @author Erwan Mahe
 */
public class BasicBroadcastTest {

    @BeforeEach
    public void before(@TempDir Path tempDir) {
        clearParameters();

        setParameter("MAX_CORE_SIMULATION_STEP", BigDecimal.ONE.toString());
        setParameter("MAX_CORE_UI_MODE", "Silent");
        setParameter("MAX_CORE_RESULTS_FOLDER_NAME", tempDir.toString());
    }

    @Test
    public void testBroadcast(TestInfo testInfo) throws Throwable {

        boolean printLogs = true;

        final var tester =
                new ExperimenterAgent() {

                    private BrachaBrbEnvironment<Integer, BroadcastPeerAgent, DefaultLocalBrbNodeState<Integer>> Benv;
                    private StochasticP2PEnvironment P2Penv;

                    @Override
                    protected List<MAXAgent> setupScenario() {
                        final List<MAXAgent> agents = new ArrayList<>();

                        P2Penv = new StochasticP2PEnvironment();
                        agents.add(P2Penv);

                        Benv = new BrachaBrbEnvironment<Integer, BroadcastPeerAgent, DefaultLocalBrbNodeState<Integer>>(P2Penv.getName());
                        agents.add(Benv);

                        for (int i=0;i<3;i++) {
                            BroadcastPeerAgent peer = new BroadcastPeerAgent(createPeerPlan(P2Penv.getName(), Benv.getName(),false,1));
                            //peer1.getLogger().setLevel(Level.FINE);
                            peer.setName("brachaPeer" + String.valueOf(i));
                            agents.add(peer);
                        }

                        BroadcastPeerAgent peer = new BroadcastPeerAgent(createPeerPlan(P2Penv.getName(), Benv.getName(),true,1));
                        peer.setName("brachaPeer4-sender");
                        peer.getLogger().setLevel(Level.FINE);
                        agents.add(peer);

                        return agents;
                    }

                    private Plan<BroadcastPeerAgent> createPeerPlan(String P2PenvName, String BenvName, boolean isSender, int byzantineThresholdF) {

                        final List<ActionActivator<BroadcastPeerAgent>> static_plan = new ArrayList<>();
                        final var join_time = getCurrentTick();
                        final var send_time = join_time.add(BigDecimal.valueOf(5));

                        static_plan.add(new ACTakeRole<BroadcastPeerAgent>(P2PenvName, RStochasticNetworkPeer.class, null).oneTime(join_time));
                        static_plan.add(new ACTakeRole<BroadcastPeerAgent>(BenvName, RBroadcastPeer.class, null).oneTime(join_time));
                        static_plan.add(
                                new AcAddCommunicationDelay<BroadcastPeerAgent>(BenvName,
                                        null,
                                        "OUTPUT",
                                        "BASELINE",
                                        new UniformLevelFilter(1.0),
                                        new ContinuousUniformDelay(1, 20)
                                ).oneTime(join_time));
                        static_plan.add(new AcSetPeerBroadcastLocalState<>(BenvName, null, new DefaultLocalBrbNodeState<Integer>()).oneTime(join_time));
                        static_plan.add(new AcSetBroadcastDeliveryHandler<>(BenvName, null, new TrivialTestBroadcastDeliveryHandler()).oneTime(join_time));
                        static_plan.add(new AcSetPeerBroadcastApprovalPolicy<>(BenvName, null, x -> true).oneTime(join_time));
                        static_plan.add(new AcSetPeerBroadcastByzantineThreshold<>(BenvName, null, byzantineThresholdF).oneTime(join_time));
                        if (isSender) {
                            static_plan.add(new AcStartBroadcastOfMessage<>(BenvName, null, 1).oneTime(send_time));
                        }
                        if (printLogs) {
                            static_plan.add(
                                    new AcConfigMessageLogger<BroadcastPeerAgent>(
                                            BenvName,
                                            null,
                                            "C:\\Users\\em244186\\idea_projects\\bracha_brb\\",
                                            new BrachaIntegerMessageLogWriter() {
                                            }
                                    ).oneTime(join_time)
                            );
                        }
                        return new Plan<>() {
                            @Override
                            public List<ActionActivator<BroadcastPeerAgent>> getInitialPlan() {
                                return static_plan;
                            }
                        };
                    }


                };
        launchTester(tester, 100, testInfo);
    }

}
