/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.broadcast.bracha_brb.state;

import madkit.kernel.AgentLogger;
import max.model.broadcast.bracha_brb.message.BrachaBrbMessageType;


/**
 * Default
 *
 * @author Erwan Mahe
 */
public class DefaultLocalBrbNodeState<T_msg> implements AbstractLocalBrbNodeState<T_msg> {


    public DefaultLocalBrbNodeState() {}

    @Override
    public String getDescription() {
        return "Default";
    }

    @Override
    public void onReceiveBrachaMessageAbout(T_msg carriedMessage, BrachaBrbMessageType withMessageType, AgentLogger logger) {
        // do nothing
    }

    @Override
    public void onNotifiedBroadcastDelivery(T_msg carriedMessage, AgentLogger logger) {
        // do nothing
    }

    @Override
    public DefaultLocalBrbNodeState<T_msg> copy_as_new() {
        return new DefaultLocalBrbNodeState<>();
    }

}
