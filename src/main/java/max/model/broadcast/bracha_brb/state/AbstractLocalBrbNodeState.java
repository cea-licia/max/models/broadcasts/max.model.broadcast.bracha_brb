/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/


package max.model.broadcast.bracha_brb.state;

import madkit.kernel.AgentLogger;
import max.model.broadcast.bracha_brb.message.BrachaBrbMessageType;


/**
 * Interface for keeping track of additional information (application specific)
 * related to the reception of INIT, ECHO and READY messages.
 *
 * It can be used to compute more subtle forms of order fairness on distributed ledgers which
 * rely on BRB as a broadcast primitive.
 *
 * @author Erwan Mahe
 */
public interface AbstractLocalBrbNodeState<T_msg> {

    String getDescription();

    void onReceiveBrachaMessageAbout(T_msg carriedMessage, BrachaBrbMessageType withMessageType, AgentLogger logger);

    void onNotifiedBroadcastDelivery(T_msg carriedMessage, AgentLogger logger);

    AbstractLocalBrbNodeState<T_msg> copy_as_new();

}
