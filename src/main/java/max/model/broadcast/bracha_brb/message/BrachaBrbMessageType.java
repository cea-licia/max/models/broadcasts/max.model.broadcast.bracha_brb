/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.broadcast.bracha_brb.message;

/**
 * The three message types for the Bracha Byzantine Reliable Broadcast algorithm.
 *
 * @author Erwan Mahe
 */
public enum BrachaBrbMessageType {
    INIT,
    ECHO,
    READY;

    @Override
    public String toString() {
        switch (this) {
            case INIT:
                return "BRACHA_BRB_INIT";
            case ECHO:
                return "BRACHA_BRB_ECHO";
            case READY:
                return "BRACHA_BRB_READY";
        }
        throw new RuntimeException("unexpected");
    }
}
