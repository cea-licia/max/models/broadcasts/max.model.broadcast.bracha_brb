/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.broadcast.bracha_brb.message;


import madkit.kernel.AgentAddress;
import max.datatype.com.Message;
import max.model.network.stochastic_adversarial_p2p.msglog.AbstractMessageLogWriter;


/**
 * So that any Bracha Peer may log Bracha messages in a log file.
 * **/
public abstract class BrachaAbstractMessageLogWriter<T_msg> implements AbstractMessageLogWriter {


    public abstract String innerMessageToString(T_msg msg);

    @Override
    public String messageToString(Message<AgentAddress, ?> message) {
        T_msg payload = (T_msg) message.getPayload();
        String msgType = message.getType();
        String sender = message.getSender().getAgent().getName();
        return String.format(
                "%s(%s,%s)",
                msgType,
                sender,
                this.innerMessageToString(payload)
        );
    }

}
