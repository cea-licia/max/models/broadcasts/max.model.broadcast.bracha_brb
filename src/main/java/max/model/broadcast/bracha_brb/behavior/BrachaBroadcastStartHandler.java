/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.broadcast.bracha_brb.behavior;


import max.model.broadcast.abstract_reliable_broadcast.action.AcBroadcastToOtherPeersAndImmediatelyToOneself;
import max.model.broadcast.abstract_reliable_broadcast.agent.BroadcastPeerAgent;
import max.model.broadcast.abstract_reliable_broadcast.behavior.ReliableBroadcastHandler;
import max.model.broadcast.bracha_brb.message.BrachaBrbMessageType;


/**
 * Start of the Bracha BRB procedure : the peer that wants to reliably broadcast the message starts
 * by simply broadcasting an "INIT" message carrying the message payload.
 *
 * @author Erwan Mahe
 */
public class BrachaBroadcastStartHandler<T_msg, A extends BroadcastPeerAgent> implements ReliableBroadcastHandler<T_msg, A> {

    @Override
    public void handle(T_msg message, A owner, String relevantEnvironment) {
        (new AcBroadcastToOtherPeersAndImmediatelyToOneself<>(
                relevantEnvironment,
                owner,
                message,
                BrachaBrbMessageType.INIT.toString(),
                BrachaBrbMessageType.INIT.toString()
        )).execute();
    }

}
