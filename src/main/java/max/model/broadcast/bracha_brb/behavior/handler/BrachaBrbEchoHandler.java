/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.broadcast.bracha_brb.behavior.handler;


import madkit.kernel.AgentAddress;
import max.datatype.com.Message;
import max.model.broadcast.abstract_reliable_broadcast.action.AcBroadcastToOtherPeersAndImmediatelyToOneself;
import max.model.broadcast.abstract_reliable_broadcast.agent.BroadcastPeerAgent;
import max.model.broadcast.bracha_brb.env.BrachaBrbPeerContext;
import max.model.broadcast.bracha_brb.message.BrachaBrbMessageType;
import max.model.broadcast.bracha_brb.state.AbstractLocalBrbNodeState;
import max.model.network.stochastic_adversarial_p2p.context.StochasticP2PContext;
import max.model.network.stochastic_adversarial_p2p.context.handler.OnReceiveMessageHandler;

import java.util.HashMap;
import java.util.HashSet;

/**
 * Message handler that handles BrachaBrbMessageType#Echo messages.
 *
 * @author Erwan Mahe
 */
public class BrachaBrbEchoHandler<
        T_msg,
        A extends BroadcastPeerAgent,
        T_sta extends AbstractLocalBrbNodeState<T_msg>> implements OnReceiveMessageHandler {

    @Override
    public void handle(StochasticP2PContext context, Message<AgentAddress, ?> message) {
        T_msg payload = (T_msg) message.getPayload();
        BrachaBrbPeerContext<T_msg,A,T_sta> bcontext = (BrachaBrbPeerContext<T_msg,A,T_sta>) context;
        // ***
        bcontext.localState.onReceiveBrachaMessageAbout(payload,BrachaBrbMessageType.ECHO,context.getOwner().getLogger());
        // ***
        if (bcontext.alreadyReliablyDelivered.contains(payload)) {
            context.getOwner().getLogger().fine("peer " + bcontext.getOwner().getName() + " already DELIVERED old message");
            return;
        }
        if (bcontext.forWhichReadyHasBeenBroadcast.contains(payload)) {
            context.getOwner().getLogger().fine("peer " + bcontext.getOwner().getName() + " already READIED old message");
            return;
        }
        if (!bcontext.approvalPolicy.apply(payload)) {
            context.getOwner().getLogger().warning("peer " + bcontext.getOwner().getName() + " does not approve message to READY it");
            return;
        }

        boolean broadcastReady = false;
        if (bcontext.msgCounter.containsKey(payload)) {
            HashMap<BrachaBrbMessageType, HashSet<AgentAddress>> count = bcontext.msgCounter.get(payload);
            HashSet<AgentAddress> gotEchoes = count.get(BrachaBrbMessageType.ECHO);
            gotEchoes.add(message.getSender());
            if (gotEchoes.size() >= bcontext.getEchoThreshold()) {
                broadcastReady = true;
            }
            count.put(BrachaBrbMessageType.ECHO,gotEchoes);
            bcontext.msgCounter.put(payload,count);
        } else {
            HashMap<BrachaBrbMessageType,HashSet<AgentAddress>> count = new HashMap<>();
            count.put(BrachaBrbMessageType.INIT,new HashSet<>());
            HashSet<AgentAddress> gotFrom = new HashSet<>();
            gotFrom.add(message.getSender());
            count.put(BrachaBrbMessageType.ECHO,gotFrom);
            count.put(BrachaBrbMessageType.READY,new HashSet<>());
            bcontext.msgCounter.put(payload,count);
        }

        if (broadcastReady) {
            bcontext.forWhichReadyHasBeenBroadcast.add(payload);
            (new AcBroadcastToOtherPeersAndImmediatelyToOneself<>(
                    bcontext.getEnvironmentName(),
                    (BroadcastPeerAgent) bcontext.getOwner(),
                    payload,
                    BrachaBrbMessageType.READY.toString(),
                    BrachaBrbMessageType.READY.toString()
            )).execute();
        }
    }

}
