/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.broadcast.bracha_brb.action;

import max.core.action.Action;
import max.model.broadcast.abstract_reliable_broadcast.agent.BroadcastPeerAgent;
import max.model.broadcast.abstract_reliable_broadcast.role.RBroadcastPeer;
import max.model.broadcast.bracha_brb.env.BrachaBrbPeerContext;
import max.model.broadcast.bracha_brb.state.AbstractLocalBrbNodeState;

import java.util.function.Function;


/**
 * Action executed when (re)setting the peer approval policy which dictates whether or not it will echo and ready messages.
 *
 * @author Erwan Mahe
 */
public class AcSetPeerBroadcastApprovalPolicy<
        T_msg,
        A extends BroadcastPeerAgent,
        T_sta extends AbstractLocalBrbNodeState<T_msg>> extends Action<A> {

    private final Function<T_msg,Boolean> approvalPolicy;

    public AcSetPeerBroadcastApprovalPolicy(String environmentName, A owner, Function<T_msg,Boolean> approvalPolicy) {
        super(environmentName, RBroadcastPeer.class, owner);
        this.approvalPolicy = approvalPolicy;
    }

    @Override
    public void execute() {
        BrachaBrbPeerContext<T_msg,A,T_sta> context = (BrachaBrbPeerContext<T_msg,A,T_sta>) this.getOwner().getContext(this.getEnvironment());
        context.approvalPolicy = this.approvalPolicy;
    }

    @Override
    public <T extends Action<A>> T copy() {
        return (T) new AcSetPeerBroadcastApprovalPolicy(getEnvironment(), getOwner(), this.approvalPolicy);
    }
}
