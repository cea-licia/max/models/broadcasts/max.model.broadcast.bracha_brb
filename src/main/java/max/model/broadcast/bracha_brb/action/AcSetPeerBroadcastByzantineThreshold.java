/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.broadcast.bracha_brb.action;

import max.core.action.Action;
import max.model.broadcast.abstract_reliable_broadcast.agent.BroadcastPeerAgent;
import max.model.broadcast.abstract_reliable_broadcast.role.RBroadcastPeer;
import max.model.broadcast.bracha_brb.env.BrachaBrbPeerContext;
import max.model.broadcast.bracha_brb.state.AbstractLocalBrbNodeState;


/**
 * Action executed when (re)setting the peer byzantine threshold which dictates
 * the number of ECHOes required to emit a READY
 * the number of READYs required to emit a READY (if not already done)
 * the number of READYs required to deliver the message.
 *
 * @author Erwan Mahe
 */
public class AcSetPeerBroadcastByzantineThreshold<
        T_msg,
        A extends BroadcastPeerAgent,
        T_sta extends AbstractLocalBrbNodeState<T_msg>> extends Action<A> {

    private final Integer byzantineThresholdF;

    public AcSetPeerBroadcastByzantineThreshold(String environmentName, A owner, Integer byzantineThresholdF) {
        super(environmentName, RBroadcastPeer.class, owner);
        this.byzantineThresholdF = byzantineThresholdF;
    }

    @Override
    public void execute() {
        BrachaBrbPeerContext<T_msg,A,T_sta> context = (BrachaBrbPeerContext<T_msg,A,T_sta>) this.getOwner().getContext(this.getEnvironment());
        context.byzantineThresholdF = this.byzantineThresholdF;
    }

    @Override
    public <T extends Action<A>> T copy() {
        return (T) new AcSetPeerBroadcastByzantineThreshold(getEnvironment(), getOwner(), this.byzantineThresholdF);
    }
}
