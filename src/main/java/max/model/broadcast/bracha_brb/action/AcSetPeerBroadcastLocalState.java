/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.broadcast.bracha_brb.action;

import max.core.action.Action;
import max.model.broadcast.abstract_reliable_broadcast.agent.BroadcastPeerAgent;
import max.model.broadcast.abstract_reliable_broadcast.role.RBroadcastPeer;
import max.model.broadcast.bracha_brb.env.BrachaBrbPeerContext;
import max.model.broadcast.bracha_brb.state.AbstractLocalBrbNodeState;

import java.util.function.Function;


/**
 * Action executed when (re)setting the BRB broadcast peer local state.
 *
 * @author Erwan Mahe
 */
public class AcSetPeerBroadcastLocalState<
        T_msg,
        A extends BroadcastPeerAgent,
        T_sta extends AbstractLocalBrbNodeState<T_msg>> extends Action<A> {

    private final T_sta localState;

    public AcSetPeerBroadcastLocalState(String environmentName, A owner, T_sta localState) {
        super(environmentName, RBroadcastPeer.class, owner);
        this.localState = localState;
    }

    @Override
    public void execute() {
        BrachaBrbPeerContext<T_msg,A,T_sta> context = (BrachaBrbPeerContext<T_msg,A,T_sta>) this.getOwner().getContext(this.getEnvironment());
        getOwner().getLogger().info("(Re)initializing application-side BRB broadcast peer state");
        if (context.localState == null) {
            getOwner().getLogger().info(
                    "initializing BRB broadcast peer state of type :" + this.localState.getDescription()
            );
        } else {
            getOwner().getLogger().warning(
                    "RESET BRB broadcast peer state of type :" + this.localState.getDescription()
            );
        }
        context.localState = this.localState;
    }

    @Override
    public <T extends Action<A>> T copy() {
        return (T) new AcSetPeerBroadcastLocalState(getEnvironment(), getOwner(), this.localState.copy_as_new());
    }
}
