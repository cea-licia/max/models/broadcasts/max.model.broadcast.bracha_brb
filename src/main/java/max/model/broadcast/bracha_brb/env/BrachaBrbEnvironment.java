/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.broadcast.bracha_brb.env;

import max.core.Context;
import max.core.agent.SimulatedAgent;
import max.model.broadcast.abstract_reliable_broadcast.agent.BroadcastPeerAgent;
import max.model.broadcast.abstract_reliable_broadcast.env.AbstractBroadcastEnvironment;
import max.model.broadcast.bracha_brb.state.AbstractLocalBrbNodeState;

/**
 * An environment for reliable broadcasts.
 *
 * @author Erwan Mahe
 */
public class BrachaBrbEnvironment<
        T_msg,
        A extends BroadcastPeerAgent,
        T_sta extends AbstractLocalBrbNodeState<T_msg>> extends AbstractBroadcastEnvironment<T_msg,A> {


    public BrachaBrbEnvironment(String messageDeliveryEnvironment) {
        super(messageDeliveryEnvironment);
    }

    @Override
    protected Context createContext(SimulatedAgent agent) {
        return new BrachaBrbPeerContext<T_msg,A,T_sta>((BroadcastPeerAgent) agent, this);
    }

}
