/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.broadcast.bracha_brb.env;

import madkit.kernel.AgentAddress;
import max.model.broadcast.abstract_reliable_broadcast.agent.BroadcastPeerAgent;
import max.model.broadcast.abstract_reliable_broadcast.behavior.ReliableBroadcastHandler;
import max.model.broadcast.abstract_reliable_broadcast.env.BroadcastPeerContext;
import max.model.broadcast.bracha_brb.behavior.handler.BrachaBrbEchoHandler;
import max.model.broadcast.bracha_brb.behavior.handler.BrachaBrbInitHandler;
import max.model.broadcast.bracha_brb.behavior.BrachaBroadcastStartHandler;
import max.model.broadcast.bracha_brb.behavior.handler.BrachaBrbReadyHandler;
import max.model.broadcast.bracha_brb.message.BrachaBrbMessageType;
import max.model.broadcast.bracha_brb.state.AbstractLocalBrbNodeState;
import max.model.network.stochastic_adversarial_p2p.env.StochasticP2PEnvironment;


import java.util.HashMap;
import java.util.HashSet;
import java.util.function.Function;


/**
 * Context for peers in a broadcast environment.
 *
 * @author Erwan Mahe
 */
public class BrachaBrbPeerContext<
        T_msg,
        A extends BroadcastPeerAgent,
        T_sta extends AbstractLocalBrbNodeState<T_msg>> extends BroadcastPeerContext<T_msg,A> {


    public T_sta localState;

    /**
     * Dictates whether or not the node should approve the message that is to be broadcast.
     * If so it will ECHO and READY it
     * If not it will not do so (but if more than 2*f+1 other nodes READY it, it will still consider it delivered)
     * **/
    public Function<T_msg,Boolean> approvalPolicy;

    /**
     * The (integer) number of potential Byzantine nodes used in computing the thresholds for the ECHO and READY quorums
     * **/
    public Integer byzantineThresholdF;

    /**
     * Keeps track, for each message proposal, of the ECHOs and READYs it has received from the other nodes
     * **/
    public HashMap<T_msg, HashMap<BrachaBrbMessageType, HashSet<AgentAddress>>> msgCounter;

    /**
     * Keeps track of messages for which the node has already emitted a corresponding READY
     * **/
    public HashSet<T_msg> forWhichReadyHasBeenBroadcast;

    /**
     * Keeps track of messages that have already been reliably delivered
     * **/
    public HashSet<T_msg> alreadyReliablyDelivered;

    public BrachaBrbPeerContext(BroadcastPeerAgent owner, StochasticP2PEnvironment environment) {
        super(owner, environment);
        this.byzantineThresholdF = null;
        this.alreadyReliablyDelivered = new HashSet<>();
        this.forWhichReadyHasBeenBroadcast = new HashSet<>();
        this.msgCounter = new HashMap<>();
        this.startHandler = (ReliableBroadcastHandler<T_msg, A>) new BrachaBroadcastStartHandler<T_msg,A>();
        this.setOnReceiveHandlerForMessageType(BrachaBrbMessageType.INIT.toString(),new BrachaBrbInitHandler<T_msg,A,T_sta>());
        this.setOnReceiveHandlerForMessageType(BrachaBrbMessageType.ECHO.toString(),new BrachaBrbEchoHandler<T_msg,A,T_sta>());
        this.setOnReceiveHandlerForMessageType(BrachaBrbMessageType.READY.toString(),new BrachaBrbReadyHandler<T_msg,A,T_sta>());
    }


    /**
     * See alpha in https://inria.hal.science/hal-03347874/document and Figure 1 and Table 1.
     * **/
    public int getEchoThreshold() {
        // given n = 3*f + 1
        // and alpha = (n+f)/2 + 1
        // we have alpha = (4*f+1)/2 +1 = 2*f + 1.5 rounded to 2*f + 1
        return 2*this.byzantineThresholdF + 1;
    }

    /**
     * See beta in https://inria.hal.science/hal-03347874/document and Figure 1 and Table 1.
     * **/
    public int getReadyThreshold() {
        return this.byzantineThresholdF + 1;
    }

    /**
     * See gamma in https://inria.hal.science/hal-03347874/document and Figure 1 and Table 1.
     * **/
    public int getDeliverThreshold() {
        return 2*this.byzantineThresholdF + 1;
    }

}
