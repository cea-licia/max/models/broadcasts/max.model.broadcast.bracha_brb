open module max.model.broadcast.bracha_brb {
    // Exported packages
    exports max.model.broadcast.bracha_brb.action;
    exports max.model.broadcast.bracha_brb.behavior;
    exports max.model.broadcast.bracha_brb.behavior.handler;
    exports max.model.broadcast.bracha_brb.env;
    exports max.model.broadcast.bracha_brb.message;
    exports max.model.broadcast.bracha_brb.state;

    // Dependencies
    requires java.logging;
    requires java.desktop;
    requires org.apache.commons.lang3;

    requires madkit;      // Automatic

    requires max.core;
    requires max.datatype.com;
    requires max.model.network.stochastic_adversarial_p2p;
    requires max.model.broadcast.abstract_reliable_broadcast;

    // Optional dependency (required for tests)
    requires static org.junit.jupiter.api;

}