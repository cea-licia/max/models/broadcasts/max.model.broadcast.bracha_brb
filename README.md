# max.model.broadcast.bracha_brb

This implements the interface 
[max.model.broadcast.abstract_reliable_broadcast](https://gitlab.com/cea-licia/max/models/broadcasts/max.model.broadcast.abstract_reliable_broadcast) 
via the Bracha Byzantine Reliable Broadcast algorithm.

See [this paper](https://inria.hal.science/hal-03347874/document).

## The algorithm

The algorithm is the following :

<img src="./README_images/bracha_algo.png">

(image above from [this paper](https://inria.hal.science/hal-03347874/document))

It relies on two "echoing" phases to ascertain that all peers agree on the same message that is broadcast.

The peer that wants to initiate the reliable broadcast start with a primitive broadcast (simple 1 to N)
of an "INIT" message carrying the message it wants to reliably broadcast.

The first echoing phase then consists in peers emitting and collecting "ECHO" messages that are answers to the "INIT" message.

The second echoing phase likewise consists in collecting "READY" messages.

Once enough READY messages have been received locally by a given peer, the peer can deliver the reliably broadcast message
because it is then certain that all the other peers will also eventually deliver that same message.

The threshold for the collection of ECHO and READY messages depends on the maximum number of byzantine peers "t":


<img src="./README_images/bracha_threshold.png">

(image above from [this paper](https://inria.hal.science/hal-03347874/document))


## The implementation of the model

This model implements the abstract reliable broadcast interface from [max.model.broadcast.abstract_reliable_broadcast](https://gitlab.com/cea-licia/max/models/broadcasts/max.model.broadcast.abstract_reliable_broadcast).

It uses primitive broadcasts to exchanges INIT, ECHO and READY messages.
These primitive broadcast are implemented using the Peer To Peer model from
[max.model.network.stochastic_adversarial_p2p](https://gitlab.com/cea-licia/max/models/networks/max.model.network.stochastic_adversarial_p2p).
Hence, we can also use the associated adversarial model to easily implement attacks by an adversary.


In the "BrachaBrbPeerContext" (context of the agent participating in the Bracha instance) we also have added
a "localState" attribute which implements a "AbstractLocalBrbNodeState" interface.
It can be set via "AcSetPeerBroadcastLocalState".
During the execution of the protocol, method "onReceiveBrachaMessageAbout(carriedMessage, withMessageType, ...)"
is called whenever a INIT, ECHO, or READY message is received concerning a specific message.
This can be used to e.g., to keep track, on each individual peer, of the order with which:
- messages are initially proposed (reception of INIT)
- messages are delivered (reception of 2*f+1 READY)


## Example execution on a simplistic example

We consider 4 peers:
- 1 peer with a FINE log level (to better see the logging displayed during simulations) that will initiate an instance of reliable broadcast
- 3 peers with default INFO log level (not to clutter to much the displayed log)

We consider 2 environments in which these 4 peers exist:
- the delivery environment which is application-side and on which the delivery of reliably broadcast messages has an effect
- the Bracha BRB environment in which the Bracha BRB protocol takes place

The message that is reliably broadcast is simply the integer value "1".
Once delivered, this message will only write "peer *peer-name* got message 1 on environment *delivery-environment*"

To run the simulation, launch the test in "BasicBroadcastTest".

We parameterize Uniform Random Delays between each atomic emission event and the corresponding atomic reception event
to represents the non-determinism and variability of the network.

At first we have the setup:

<img src="./README_images/setup.png">

Then the initiating peer triggers the start of the reliable broadcast instance.
Because it immediately receives the "INIT" it has itself send, it also immediately sends its "ECHO" message:

<img src="./README_images/init.png">

Once the other peers start receiving the "INIT" message, they reply with "ECHO" messages:

<img src="./README_images/echo.png">

A first peer, having received enough "ECHO" messages replies with the first "READY" message:

<img src="./README_images/ready1.png">

Then, all the other peers progressively also do so:

<img src="./README_images/ready2.png">

Finally, the message is delivered on each peer (not necessarily at the same time but rather progressively):

<img src="./README_images/delivery.png">

### Logs of input and output events

Via the API from [max.model.network.stochastic_adversarial_p2p](https://gitlab.com/cea-licia/max/models/networks/max.model.network.stochastic_adversarial_p2p),
it is also possible to generate log files that only relate to the atomic message input and output events
that occur on each node.

For instance, below, we have 4 log files, each
corresponding to a node in the previous example.

<img src="./README_images/logs.png"> 


# Licence

__max.model.broadcast.bracha_brb__ is released under
the [Eclipse Public Licence 2.0 (EPL 2.0)](https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html)













